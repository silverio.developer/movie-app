//
//  MovieUserMeta.swift
//  MovieSwift
//
//  Created by Rodrigo Silverio on 28/06/2019.
//  Copyright © 2019 Rodrigo Silverio. All rights reserved.
//

import Foundation

struct MovieUserMeta: Codable {
    var addedToList: Date?
}
