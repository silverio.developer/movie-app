//
//  Keyword.swift
//  MovieSwift
//
//  Created by Rodrigo Silverio on 16/06/2019.
//  Copyright © 2019 Rodrigo Silverio. All rights reserved.
//

import Foundation
import SwiftUI

struct Keyword: Codable, Identifiable {
    let id: Int
    let name: String
}

