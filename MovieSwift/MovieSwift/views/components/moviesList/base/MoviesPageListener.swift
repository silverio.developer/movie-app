//
//  MoviesPageListener.swift
//  MovieSwift
//
//  Created by Rodrigo Silverio on 09/07/2019.
//  Copyright © 2019 Rodrigo Silverio. All rights reserved.
//

import Foundation

class MoviesPagesListener {
    var currentPage: Int = 1 {
        didSet {
            loadPage()
        }
    }
    
    func loadPage() {
        
    }
}
